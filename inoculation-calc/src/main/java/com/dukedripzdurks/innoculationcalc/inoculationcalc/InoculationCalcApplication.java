package com.dukedripzdurks.innoculationcalc.inoculationcalc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InoculationCalcApplication {

	public static void main(String[] args) {
		SpringApplication.run(InoculationCalcApplication.class, args);
	}

}
